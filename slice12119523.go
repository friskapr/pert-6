package main

import "fmt"

func main() {
	var kursus = []string{"dbms", "sever os", "networking",
		"web", "desktop", "erp"}
	var kursus_saya = []string{"sever os", "networking",
		"web", "desktop", "Manajemen Informatika"}

	fmt.Println("Isi kursus adalah", kursus)
	fmt.Print("Panjang kursus ", len(kursus))
	fmt.Println(" dan kapasitas", cap(kursus))

	fmt.Println("Isi kursus adalah", kursus_saya)
	fmt.Print("Panjang kursus ", len(kursus_saya))
	fmt.Println(" dan kapasitas", cap(kursus_saya))

}
