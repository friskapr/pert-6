package main

import "fmt"

func main() {
	var gajiSekarang, ekspektasiGaji int
	fmt.Print("Masukan Gaji Anda = ")
	fmt.Scan(&gajiSekarang)

	fmt.Print("Masukkan Gaji Yang Anda Inginkan = ")
	fmt.Scan(&ekspektasiGaji)

	naikanGaji(&gajiSekarang, ekspektasiGaji)

	fmt.Printf("\nGaji Anda Sekarang %d\n", gajiSekarang)
}
func naikanGaji(gajiAwal *int, gajiAkhir int) {
	*gajiAwal = gajiAkhir
}
